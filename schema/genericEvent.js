const joi = require('joi');

module.exports = {
  deviceId: joi.string().required(),
  deviceVersion: joi.string().required(),
  eventTime: joi.date().required(),
  alertTime: joi.date().required(),
  dstDomain: joi.string().required(),
  dstUrl: joi.string().required(),
  protocolVersion: joi.string().required(),
  providerName: joi.string().required(),
  dstIp: joi.string(),
  eventSeverity: joi.string(),
  eventType: joi.string(),
  src: joi.string(),
};
