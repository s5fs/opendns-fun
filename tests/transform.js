const test = require('tape');
const joi = require('joi');
const transform = require('../lib/transform');
const checkpoint = require('./data/checkpoint-sample.json');
const schema = require('../schema/genericEvent');

test('Ensure our sample data is legit', (t) => {
  t.true(Array.isArray(checkpoint));
  t.end();
});

test('Convert first checkpoint data to object', (t) => {
  const event = transform(checkpoint[0]);

  t.true(typeof event === 'object');

  joi.validate(event, schema, function (error) {
    t.false(error);
    t.end();
  });
});
