const config = require('./config');
const event = require('./lib/event');
const logger = require('./lib/logger');
let amqp = require('amqplib');

logger.setTitle('Subscriber');

amqp.connect('amqp://localhost').then((conn) => {

  process.once('SIGINT', () => {
    conn.close();
  });

  return conn.createChannel().then((ch) => {
    let ok = ch.assertQueue(config.queue, {durable: false});

    ok = ok.then((_qok) => {
      return ch.consume(config.queue, (msg) => {
        const record = event.transform(msg.content.toString());

        event.send(record, (error, res) => {
          if (error) {
            return logger.error('Error during send');
          }
          logger.log(res);
        });
      }, {noAck: true});
    });

    return ok.then(function(_consumeOk) {
      logger.log(`listening`);
    });
  });
}).catch(logger.log);
