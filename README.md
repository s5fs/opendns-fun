
### OpenDNS is fun!

__Setting up for local development__

1. Clone this repo (already done I hope)
2. `npm install` all the things
3. `npm test` to ensure nothing blows up
4. Well.. we're also using RabbitMQ so maybe you wanna install that too

__Using Vagrant__

We're built a simple Vagrant box that's ready for development, has database and node and a bunch of other stuff. Checkout the playbook.yml to see what is included.

Built and tested with Virtualbox 5.0.

1. `cp SAMPLE-config.json config.json` Edit the config to use your customerKey
2. `vagrant up` to launch the box, this may prompt for your workstation password to allow the NFS mount
3. `vagrant ssh` to login
4. `cd /vagrant; npm install` all the things
5. Start the server `node server.js`
6. (in another `vagrant ssh` session) Start the subscriber `node /vagrant/subscriber.js`
7. (in yet another `vagrant ssh` session) Apply load `cd /vagrant; ./node_modules/.bin/loadtest --rps 1000 -n 2000 -T "application/json" -p ./tests/data/checkpoint-sample.json http://opendns.fun:80`
 
__Testing with cURL__

`curl localhost:3000 -H "Content-Type: application/json" -d "@./tests/data/checkpoint-sample.json" -v`

