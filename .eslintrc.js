module.exports = {
    "extends": ["eslint:recommended", "google"],
    "env": {
        "node": true
    },
    "parserOptions": {
      "ecmaVersion": 6
    },
    "rules": {
      "require-jsdoc": 0
    }
};
