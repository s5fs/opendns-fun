const express = require('express');
const parser = require('body-parser');
const event = require('./lib/event');
const app = express();
const logger = require('./lib/logger');
const args = require('minimist')(process.argv.slice(2));

logger.setTitle('Server Process');

app.use(parser.json());

app.post('/', (req, res) => {
  if (!req.body || !Array.isArray(req.body)) {
    logger.error('no payload detected')
    return res.send(400)
  }

  event.enqueue(req.body[0]);
  res.send();
});

app.listen(args.port || 3000, () => {
  logger.log('http server started');
});
