/* eslint no-console: 0 */

let title = 'logger';

module.exports = {
  setTitle: (msg) => {
    title = msg;
  },
  log: (message) => {
    console.log(`[${title}] ${JSON.stringify(message)}`);
  },
  error: (message) => {
    console.error(`[${title}] ${JSON.stringify(message)}`);
  },
};
