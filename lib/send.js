const request = require('request');
const config = require('../config');
const logger = require('./logger');

module.exports = function(event, callback) {
  let options = {
    uri: `https://s-platform.api.opendns.com/1.0/events?customerKey=${config.customerKey}`,
    method: 'POST',
    json: event,
  };

  request(options, (error, res, body) => {
    if (error) {
      logger.error(error);
      return callback(error);
    }

    let isError = res.statusCode !== 202;
    return callback(isError, body);
  });
};
