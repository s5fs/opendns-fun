const moment = require('moment');
const url = require('url');

function getDate(data) {
  return data.split(' ')[0];
}

function str2obj(data) {
  const obj = {};
  data.split(';').map((i) => {
    if (i) {
      const el = i.split(': ');
      obj[el[0].trim()] = el[1].trim();
    }
  });

  return obj;
}

module.exports = function(data) {
  if (Array.isArray(data)) {
    data = data[0];
  }

  const fields = str2obj(data);
  const event = {
    deviceId: fields.snid,
    deviceVersion: fields.device_version,
    eventTime: moment().format(),
    alertTime: moment(getDate(data)).format(),
    dstDomain: url.parse(fields.resource).hostname,
    dstUrl: fields.resource,
    protocolVersion: '1.0a',
    providerName: 'Security Platform',
    dstIp: fields.dst,
    eventSeverity: fields.severity,
    eventType: fields['Protection Type'],
    src: fields.src,
  };

  return event;
};
