const amqp = require('amqplib/callback_api');
const logger = require('./logger');
const config = require('../config');
let channel = null;

amqp.connect('amqp://localhost', (err, conn) => {
  if (err) {
    logger.log(err);
  }

  conn.createChannel((err, ch) => {
    ch.assertQueue(config.queue, {durable: false});
    channel = ch;
  });
});

module.exports = function(event) {
  channel.sendToQueue('outbound', new Buffer(event));
};
